package pl.dmcs.yolo23.kompas;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class KompasMainWindowFragment extends Fragment {

	private GLSurfaceView glSurfaceView;
	private KompasMainWindowRenderer renderer;
    private static SensorManager sensorService;
    private Sensor sensor;
	
	public KompasMainWindowFragment() {
	}
	
    private static final String ARG_SECTION_NUMBER = "section_number";

    public static KompasMainWindowFragment newInstance() {
        KompasMainWindowFragment fragment = new KompasMainWindowFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("deprecation")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_kompas, container, false);
        
        glSurfaceView = (GLSurfaceView) rootView.findViewById(R.id.surfaceviewclass);
        
		// Check if the system supports OpenGL ES 2.0.
		final ActivityManager activityManager = (ActivityManager)
				getActivity().getSystemService(Context.ACTIVITY_SERVICE);
		final ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
		final boolean supportsEs2 = configurationInfo.reqGlEsVersion >= 0x20000;
        if (supportsEs2)
		{
			// Request an OpenGL ES 2.0 compatible context.
			glSurfaceView.setEGLContextClientVersion(2);

			// Set the renderer to our demo renderer, defined below.
			renderer = new KompasMainWindowRenderer( getActivity() );
			glSurfaceView.setRenderer( renderer );
		}
		else
		{
			// This is where you could create an OpenGL ES 1.x compatible
			// renderer if you wanted to support both ES 1 and ES 2.
			return null;
		}
        
		sensorService = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
	    sensor = sensorService.getDefaultSensor(Sensor.TYPE_ORIENTATION);
	    if (sensor != null) {
	      sensorService.registerListener( mySensorEventListener, sensor,
	          SensorManager.SENSOR_DELAY_NORMAL);
	    }
       
        return rootView;
    }
    
	@Override
	public void onResume() {
		// The activity must call the GL surface view's onResume() on activity
		// onResume().
		super.onResume();
		glSurfaceView.onResume();
	}

	@Override
	public void onPause() {
		// The activity must call the GL surface view's onPause() on activity
		// onPause().
		super.onPause();
		glSurfaceView.onPause();
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
    
    private SensorEventListener mySensorEventListener = new SensorEventListener() {

	    @Override
	    public void onAccuracyChanged(Sensor sensor, int accuracy) {
	    }

	    @Override
	    public void onSensorChanged(SensorEvent event) {
	      // angle between the magnetic north direction
	      // 0=North, 90=East, 180=South, 270=West
	      renderer.setAngle( event.values[0] );
	    }
	  };

	  @Override
	  public void onDestroy() {
	    super.onDestroy();
	    if (sensor != null) {
	      sensorService.unregisterListener(mySensorEventListener);
	    }
	  }
	
}
