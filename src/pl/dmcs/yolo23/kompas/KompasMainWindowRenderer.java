package pl.dmcs.yolo23.kompas;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.SystemClock;



public class KompasMainWindowRenderer implements GLSurfaceView.Renderer {

	private Activity parent;
	
	private float[] mModelMatrix = new float[16];
	private float[] mViewMatrix = new float[16];
	private float[] mProjectionMatrix = new float[16];
	private float[] mMVPMatrix = new float[16];

	/** Store our model data in a float buffer. */
	private final FloatBuffer mKompas, mKompasTex, mKompasNormals;

	private int mMVPMatrixHandle;
	private int mPositionHandle;
	private int mColorHandle;
	private int mMVMatrixHandle;
	private int mLightPosHandle;
	private int mTextureUniformHandle;
	private int mNormalHandle;

	/** This will be used to pass in model texture coordinate information. */
	private int mTextureCoordinateHandle;
	private final int mBytesPerFloat = 4;
	private final int mStrideBytes = 7 * mBytesPerFloat;
	private final int mPositionOffset = 0;
	private final int mPositionDataSize = 3;
	private final int mColorOffset = 3;
	private final int mColorDataSize = 4;
	private final int mTextureCoordinateDataSize = 2;
	private final int mNormalDataSize = 3;
	
	// record the compass picture angle turned
    private float currentDegree = 0f, targetDegree = 0f;
	
	private int mTextureDataHandle;
	private int mProgramHandle;
	
	public KompasMainWindowRenderer( Activity parent ) {
		
		this.parent = parent;
		
		// board
		final float[] Kompas = {
				 1.0f,  1.0f, 0.0f, 1.0f,1.0f,1.0f,1.0f,
				-1.0f,  1.0f, 0.0f, 1.0f,1.0f,1.0f,1.0f,
				 1.0f, -1.0f, 0.0f, 1.0f,1.0f,1.0f,1.0f,
				-1.0f, -1.0f, 0.0f, 1.0f,1.0f,1.0f,1.0f
		};
		
		final float[] KompasTex =
			{
				1.0f, 0.0f,
				0.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f
			};
		
		final float[] KompasNormalData =
			{
				0.0f, 0.0f, 1.0f,	
				0.0f, 0.0f, 1.0f,
				0.0f, 0.0f, 1.0f,
				0.0f, 0.0f, 1.0f
			};
		
		mKompas = ByteBuffer.allocateDirect(Kompas.length * mBytesPerFloat)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		mKompas.put(Kompas).position(0);
		
		mKompasTex = ByteBuffer.allocateDirect(KompasTex.length * mBytesPerFloat)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		mKompasTex.put(KompasTex).position(0);
		
		mKompasNormals = ByteBuffer.allocateDirect(KompasNormalData.length * mBytesPerFloat)
		        .order(ByteOrder.nativeOrder()).asFloatBuffer();	
		mKompasNormals.put(KompasNormalData).position(0);

	}

	@Override
	public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
		
		// Set the background clear color to gray.
		GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

		// Use culling to remove back faces.
		GLES20.glEnable(GLES20.GL_CULL_FACE);

		// Enable depth testing
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		
		// Position the eye behind the origin.
		final float eyeX = 0.0f;
		final float eyeY = 0.0f;
		final float eyeZ = 1.5f;

		// We are looking toward the distance
		final float lookX = 0.0f;
		final float lookY = 0.0f;
		final float lookZ = 1.0f;

		// Set our up vector. This is where our head would be pointing were we holding the camera.
		final float upX = 0.0f;
		final float upY = 1.0f;
		final float upZ = 0.0f;

		// Set the view matrix. This matrix can be said to represent the camera position.
		// NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model and
		// view matrix. In OpenGL 2, we can keep track of these matrices separately if we choose.
		Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
		
		final String vertexShader = parent.getString(R.string.VertexShader);
		final String fragmentShader = parent.getString(R.string.FragmentShader) ;
		
		final int vertexShaderHandle = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);	
		final int fragmentShaderHandle = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);	

		mProgramHandle = ShaderHelper.createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle,
		new String[] {"a_Position", "a_Color", "a_Normal", "a_TexCoordinate"});	
		
		// Load the texture
        mTextureDataHandle = KompasMainWindowRenderer
        		.loadTexture( parent.getApplicationContext(), R.drawable.kompas );
		
	}
	
	@Override
	public void onSurfaceChanged(GL10 glUnused, int width, int height) {
		// Set the OpenGL viewport to the same size as the surface.
		GLES20.glViewport(0, 0, width, height);
		
		final float ratio, left, right, bottom, top, near, far;

		if( height > width ){
			ratio = (float) width / height;
			left = -ratio;
			right = ratio;
			bottom = -1.0f;
			top = 1.0f;
			near = 1.0f;
			far = 3.0f;
		}
		else {
			ratio = (float) height / width;
			final float scale = 1.45f;
			left = -scale;
			right = scale;
			bottom = -ratio * scale;
			top = ratio * scale;
			near = 1.0f;
			far = 3.0f;
		}

		Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
	}
	
	@Override
	public void onDrawFrame(GL10 glUnused) {
		long startTime = System.currentTimeMillis();
		
//        long time = SystemClock.uptimeMillis() % 10000L;
//        float angleInDegrees = (360.0f / 10000.0f) * ((int) time);
		
		// rotation animation
		currentDegree += ( targetDegree - currentDegree ) / 32.0f ;
		
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
		
		GLES20.glUseProgram(mProgramHandle);
		
		mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_MVPMatrix");
        mMVMatrixHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_MVMatrix");
        mLightPosHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_LightPos");
        mTextureUniformHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_Texture");
        mPositionHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_Position");
        mColorHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_Color");
        mNormalHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_Normal");
        mTextureCoordinateHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_TexCoordinate");
		
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle);
        
        GLES20.glUniform1i(
        		GLES20.glGetUniformLocation(mProgramHandle, "u_Texture"), 0 );

		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, 0 , 0, 0.0f);
		Matrix.rotateM(mModelMatrix, 0, currentDegree, 0.0f, 0.0f, 1.0f);
		preparePolygon(mKompas);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
		
		try {
			long slp = 20 - ( System.currentTimeMillis() - startTime );
//			Log.i("fps", "Milis used to render frame: " + Long.toString( 20 - slp ));
			if( slp>0 ) Thread.sleep( slp );
		} catch (InterruptedException e) {}
	}

	/**
	 * Draws a triangle from the given vertex data.
	 * 
	 * @param aTriangleBuffer
	 *            The buffer containing the vertex data.
	 */
	private void preparePolygon(final FloatBuffer aTriangleBuffer) {
		// Pass in the position information
		aTriangleBuffer.position(mPositionOffset);
		GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false, mStrideBytes, aTriangleBuffer);

		GLES20.glEnableVertexAttribArray(mPositionHandle);

		// Pass in the color information
		aTriangleBuffer.position(mColorOffset);
		GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, mStrideBytes, aTriangleBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);
		
		// Pass in the normal information
		mKompasNormals.position(0);
        GLES20.glVertexAttribPointer(mNormalHandle, mNormalDataSize, GLES20.GL_FLOAT, false,
         0, mKompasNormals);
        
        GLES20.glEnableVertexAttribArray(mNormalHandle);
		
		// Pass in the texture coordinate information
        mKompasTex.position(0);
        GLES20.glVertexAttribPointer(mTextureCoordinateHandle, mTextureCoordinateDataSize, GLES20.GL_FLOAT, false,
         0, mKompasTex);
        GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);

		// This multiplies the view matrix by the model matrix, and stores the
		// result in the MVP matrix
		// (which currently contains model * view).
		Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);

		// This multiplies the modelview matrix by the projection matrix, and
		// stores the result in the MVP matrix
		// (which now contains model * view * projection).
		Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);

		GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);
        GLES20.glUniform3f(mLightPosHandle, 0, 0, 1);
	}

	private static int loadTexture(final Context context, final int resourceId) {
		final int[] textureHandle = new int[1];

		GLES20.glGenTextures(1, textureHandle, 0);

		if (textureHandle[0] != 0) {
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inScaled = false; // No pre-scaling

			// Read in the resource
			final Bitmap bitmap = BitmapFactory.decodeResource(
					context.getResources(), resourceId, options);

			// Bind to the texture in OpenGL
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

			// Set filtering
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
					GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
					GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

			// Load the bitmap into the bound texture.
			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

			// Recycle the bitmap, since its data has been loaded into OpenGL.
			bitmap.recycle();
		}

		if (textureHandle[0] == 0) {
			throw new RuntimeException("Error loading texture.");
		}

		return textureHandle[0];
	}
	
	public void setAngle( float f ) { targetDegree = f; }
	
	
}
